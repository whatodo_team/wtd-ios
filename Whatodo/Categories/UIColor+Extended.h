//
//  UIColor+Extended.h
//  TAO
//
//  Created by Gaëtan RATTIN on 07/03/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extended)

- (BOOL)isEqualToColor:(UIColor *)color withTolerance:(CGFloat)tolerance;
+ (UIColor *)wtdPrimaryColor;
+ (UIColor *)wtdSecondaryColor;

@end
