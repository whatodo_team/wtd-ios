//
//  UIViewController+Extended.m
//
//  Created by Gaëtan RATTIN on 17/06/2015.
//  Copyright (c) 2015 Phoceis. All rights reserved.
//

#import "UIViewController+Extended.h"
#import <objc/runtime.h>

@implementation UIViewController (Extended)

//Indique si la vue controller est modale...
- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self || (self.navigationController.presentingViewController.presentedViewController != nil && self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController) || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

//Animation de push back et front... Utile à appeler lors d'un present view controller...
- (void)pushBackAnimation{
    
    UIView* view = self.navigationController.view?self.navigationController.view:self.view;
    [view pushBack];
    
}

- (void)pushFrontAnimation{
    
    UIView* view = self.navigationController.view?self.navigationController.view:self.view;
    [view pushFront];
    
}




@end
