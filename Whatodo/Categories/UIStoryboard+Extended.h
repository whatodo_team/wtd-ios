//
//  UIStoryboard+Extended.h
//
//  Created by Gaëtan RATTIN on 02/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Extended)

+ (UIViewController *)instantiateInitialControllerWithStoryboardName:(NSString *)name;
+ (UIViewController*)instantiateViewControllerWithName:(NSString*)viewControllerName fromStoryBoardName:(NSString*)storyBoardName;
+ (id)getViewControllerWithIdentifier:(NSString *)viewControllerIdentifier inStoryboardWithName:(NSString *)storyboardName;

@end
