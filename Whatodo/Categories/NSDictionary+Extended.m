//
//  NSDictionary+Extended.m
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "NSDictionary+Extended.h"

@implementation NSDictionary (Extended)

//Indique si le dictionnaire contient une clé donnée...
- (BOOL)containsKey:(NSString *)key{
    
    return [[self allKeys] containsObject:key];
    
}

//Génère un json depuis un dictionnaire...
-(NSString *)jsonStringWithPrettyPrint:(BOOL)prettyPrint{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:(NSJSONWritingOptions)(prettyPrint?NSJSONWritingPrettyPrinted:0) error:&error];
    if (! jsonData) {
        //NSLog(@"jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end
