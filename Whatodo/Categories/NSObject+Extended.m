//
//  NSObject+Extended.m
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "NSObject+Extended.h"
#import <objc/runtime.h>
#import <objc/message.h>

#define kOMDateFormat @"yyyy-MM-dd'T'HH:mm:ss.SSS"
#define kOMTimeZone @"UTC"

@implementation NSObject (Extended)


//Replace NSNull par Nil au besoin...
-(id)replaceNSNullByNil{
    if([self isKindOfClass:NSNull.class]) return nil;
    return self;
}

//Renvoie le nom de la classe...
- (NSString *)className{
    return [NSString stringWithUTF8String:class_getName([self class])];
}

//Renvoie le type d'une propriété...
-(NSString *)typeFromProperty:(objc_property_t)property {
    return [[NSString stringWithUTF8String:property_getAttributes(property)] componentsSeparatedByString:@","][0];
}

//Populate des propriétés de l'objet avec l'équivalent dans le dictionnaire json...
- (void)populateWithJsonDictionary:(NSDictionary *)jsonDictionary{
    
    //On récupère le mapping dictionary...
    if (!jsonDictionary || [jsonDictionary isKindOfClass:[NSNull class]]) return;
    NSDictionary *mapDictionary = [self properties];
    for (NSString *key in [jsonDictionary allKeys]) {
        //On récupère la propriété équivalente...
        NSString *propertyName = [mapDictionary objectForKey:key];
        if (!propertyName) {
            continue;
        }
        if ([jsonDictionary objectForKey:key] == [NSNull null]) {
            //La valeur est nulle...
            [self setValue:nil forKey:propertyName];
            continue;
        }
        else if ([[jsonDictionary objectForKey:key] isKindOfClass:[NSDictionary class]]) {
            //La valeur est un dictionnaire...
            continue;
        }
        else if ([[jsonDictionary objectForKey:key] isKindOfClass:[NSArray class]]) {
            //La valeur est un array...
            continue;
        }
        else {
            //On mets la valeur dans la propriété...
            objc_property_t property = class_getProperty([self class], [propertyName UTF8String]);
            if (property) {
                NSString *classType = [self typeFromProperty:property];
                //Date...
                if ([classType isEqualToString:@"T@\"NSDate\""]) {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:kOMDateFormat];
                    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:kOMTimeZone]];
                    [self setValue:[formatter dateFromString:[jsonDictionary objectForKey:key]] forKey:propertyName];
                }
                else {
                    [self setValue:[jsonDictionary objectForKey:key] forKey:propertyName];
                }
            }
        }
    }
    
}

//Récupère un dictionnaire des propriétés pour une classe donnée...
- (NSMutableDictionary *)propertiesForClass:(Class)classObject {
    
    NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
    
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(classObject, &outCount);
    for(i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        
        NSString *pname = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        NSString *pattrs = [NSString stringWithCString:property_getAttributes(property) encoding:NSUTF8StringEncoding];
        
        pattrs = [[pattrs componentsSeparatedByString:@","] objectAtIndex:0];
        pattrs = [pattrs substringFromIndex:1];
        
        [results setObject:pattrs forKey:pname];
    }
    free(properties);
    
    if ([classObject superclass] != [NSObject class]) {
        [results addEntriesFromDictionary:[self propertiesForClass:[classObject superclass]]];
    }
    
    return results;
    
}

//Récupère les propriétés de l'objet...
- (NSDictionary *)properties{
    return [self propertiesForClass:[self class]];
}

- (void)encodeAutoWithCoder:(NSCoder *)aCoder class:(Class)classObject{
    unsigned int outCount = 0;
    objc_property_t *pt = class_copyPropertyList(classObject, &outCount);
    for (int i = 0; i < outCount; i++) {
        objc_property_t property = pt[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        
        SEL selector = NSSelectorFromString(name);
        Method mt = class_getInstanceMethod(classObject, selector);
        if (mt != NULL) {
            NSString *returnType = [classObject getMethodReturnType:mt];
            if ([returnType isEqualToString:@"i"] ||
                [returnType isEqualToString:@"q"] ||
                [returnType isEqualToString:@"Q"])
            {
                int intValue = ((int(*)(id, Method))method_invoke)(self, mt);
#if DEBUG
                //NSLog(@"Encode %@ %@ int value:%d", NSStringFromClass(classObject), name, intValue);
#endif
                [aCoder encodeInteger:intValue forKey:name];
            } else if ([returnType isEqualToString:@"I"]) {
                unsigned intValue = ((unsigned(*)(id, Method))method_invoke)(self, mt);
#if DEBUG
                //NSLog(@"Encode %@ %@ int value:%d", NSStringFromClass(classObject), name, intValue);
#endif
                [aCoder encodeInteger:intValue forKey:name];
            } else if ([returnType isEqualToString:@"f"] ||
                       [returnType isEqualToString:@"d"])
            {
                double doubleValue = ((double(*)(id, Method))method_invoke)(self, mt);
#if DEBUG
                //NSLog(@"Encode %@ %@ double value:%.f", NSStringFromClass(classObject), name, doubleValue);
#endif
                
                [aCoder encodeDouble:doubleValue forKey:name];
            } else if ([returnType isEqualToString:@"c"] ||
                       [returnType isEqualToString:@"B"])
            {
                BOOL boolValue = ((char(*)(id, Method))method_invoke)(self, mt);
#if DEBUG
                //NSLog(@"Encode %@ %@ BOOL value:%d", NSStringFromClass(classObject), name, boolValue);
#endif
                [aCoder encodeBool:boolValue forKey:name];
            } else {
                @try {
                    id value = ((id(*)(id, Method))method_invoke)(self, mt);
#if DEBUG
                    //NSLog(@"Encode %@ %@  value:%@", NSStringFromClass(classObject), name, value);
#endif
                    [aCoder encodeObject:value forKey:name];
                }
                @catch (NSException *exception) {
#if DEBUG
                    //NSLog(@"Encode Return Value Type undefined in %@", NSStringFromClass(classObject));
#endif
                }
                @finally {
                }
            }
        }
    }
    free(pt);
    Class superClass = class_getSuperclass(classObject);
    if (superClass != nil && ![superClass isEqual:[NSObject class]]){
        [self encodeAutoWithCoder:aCoder class:superClass];
    }
    
}

- (void)encodeAutoWithCoder:(NSCoder *)aCoder{
    [self encodeAutoWithCoder:aCoder class:[self class]];
}

- (void)decodeAutoWithCoder:(NSCoder *)aDecoder class:(Class)classObject{
    unsigned int outCount = 0;
    objc_property_t *pt = class_copyPropertyList(classObject, &outCount);
    for (int i = 0; i< outCount; i++) {
        objc_property_t property = pt[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        
        SEL selector = NSSelectorFromString([classObject getSetMethodName:name]);
        Method mt = class_getInstanceMethod(classObject, selector);
        if (mt != NULL) {
            NSString *argumentType = [classObject getMethodArgumentType:mt index:2];
            if ([argumentType isEqualToString:@"i"] ||
                [argumentType isEqualToString:@"q"] ||
                [argumentType isEqualToString:@"Q"])
            {
                NSInteger intValue = [aDecoder decodeIntegerForKey:name];
                void (*method_invokeTyped)(id self, Method mt, NSInteger value) = (void*)method_invoke;
                method_invokeTyped(self, mt, intValue);
#if DEBUG
                //NSLog(@"Decode %@ %@  intValue:%ld", NSStringFromClass(classObject), name, (long)intValue);
#endif
            } else if ([argumentType isEqualToString:@"I"]) {
                NSUInteger uIntValue = [aDecoder decodeIntegerForKey:name];
                void (*method_invokeTyped)(id self, Method mt, NSUInteger value) = (void*)method_invoke;
                method_invokeTyped(self, mt, uIntValue);
#if DEBUG
                //NSLog(@"Decode %@ %@   unsigned intValue:%lu", NSStringFromClass(classObject), name, (unsigned long)uIntValue);
#endif
            } else if ([argumentType isEqualToString:@"f"] || [argumentType isEqualToString:@"d"]) {
                double doubleValue = [aDecoder decodeDoubleForKey:name];
                void (*method_invokeTyped)(id self, Method mt, double value) = (void*)method_invoke;
                method_invokeTyped(self, mt, doubleValue);
#if DEBUG
                //NSLog(@"Decode %@ %@  doubleValue:%f", NSStringFromClass(classObject), name, doubleValue);
#endif
            } else if ([argumentType isEqualToString:@"c"] ||
                       [argumentType isEqualToString:@"B"])
            {
                BOOL boolValue = [aDecoder decodeBoolForKey:name];
                void (*method_invokeTyped)(id self, Method mt, BOOL value) = (void*)method_invoke;
                method_invokeTyped(self, mt, boolValue);
#if DEBUG
                //NSLog(@"Decode %@ %@  boolValue:%d", NSStringFromClass(classObject), name, boolValue);
#endif
            } else if ([argumentType isEqualToString:@"@"]) {
                NSString *value = [aDecoder decodeObjectForKey:name];
                void (*method_invokeTyped)(id self, Method mt, NSString *value) = (void*)method_invoke;
                method_invokeTyped(self, mt, value);
#if DEBUG
                //NSLog(@"Decode %@ %@  strValue:%@", NSStringFromClass(classObject), name, value);
#endif
            } else {
                @try {
                    id value = [aDecoder decodeObjectForKey:name];
                    if (value != nil){
                        void (*method_invokeTyped)(id self, Method mt, NSString *value) = (void*)method_invoke;
                        method_invokeTyped(self, mt, value);
                    }
#if DEBUG
                    //NSLog(@"Decode %@ %@  value:%@", NSStringFromClass(classObject), name, value);
#endif
                }
                @catch (NSException *exception) {
#if DEBUG
                    //NSLog(@"Decode Argument Value Type undefined in %@", NSStringFromClass(classObject));
#endif
                }
                @finally {
                }
            }
        }
    }
    free(pt);
    Class superClass = class_getSuperclass(classObject);
    if (superClass != nil && ![superClass isEqual:[NSObject class]]){
        [self decodeAutoWithCoder:aDecoder class:superClass];
    }
}

- (void)decodeAutoWithCoder:(NSCoder *)aDecoder{
    [self decodeAutoWithCoder:aDecoder class:[self class]];
}

#pragma mark - private

+ (NSString *)getMethodReturnType:(Method)mt
{
    char dstType[10] = {0};
    size_t dstTypeLen = 10;
    method_getReturnType(mt, dstType, dstTypeLen);
    return [NSString stringWithUTF8String:dstType];
}

+ (NSString *)getMethodArgumentType:(Method)mt index:(NSInteger)index
{
    char dstType[10] = {0};
    size_t dstTypeLen = 10;
    method_getArgumentType(mt, (unsigned)index, dstType, dstTypeLen);
    return [NSString stringWithUTF8String:dstType];
}

+ (NSString *)getSetMethodName:(NSString *)propertyName
{
    if ([propertyName length] == 0)
        return @"";
    
    NSString *firstChar = [propertyName substringToIndex:1];
    firstChar = [firstChar uppercaseString];
    NSString *lastName = [propertyName substringFromIndex:1];
    return [NSString stringWithFormat:@"set%@%@:", firstChar, lastName];
}


@end
