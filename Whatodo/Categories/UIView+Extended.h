//
//  UIView+Extended.h
//
//  Created by Gaëtan RATTIN on 08/11/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    UIViewBordersStyle_RoundedBorders = 1,
    UIViewBordersStyle_RoundedBordersOnTop = 2,
    UIViewBordersStyle_RoundedBordersOnBottom = 3,
    UIViewBordersStyle_NoBorders = 0
} UIViewBordersStyle;

@interface UIView (Extended)

@property UIViewBordersStyle bordersStyle;
//@property BOOL layerMaskIsSet;
- (void)doCircle;
- (void)doCircleWithBorderColor:(UIColor *)color width:(CGFloat)width withShadow:(BOOL)withShadow shadowColor:(UIColor *)shadowColor;
- (void)doRoundedShadowedView;
- (void)doRoundedBordersWithBorderColor:(UIColor *)color width:(CGFloat)width cornerRadius:(CGFloat)corderRadius;
- (void)doRectangleWithBorderColor:(UIColor *)color width:(CGFloat)width;
- (void)doSoftShadowWithColor:(UIColor *)color shadowRadius:(CGFloat)shadowRadius;
- (void)doSoftShadowUsingCGPathWithColor:(UIColor *)color shadowRadius:(CGFloat)shadowRadius;
- (void)addBorderWithColor:(UIColor *)color borderWidth:(CGFloat)borderWidth onTop:(BOOL)onTop onBottom:(BOOL)onBottom;
- (void)addBorderWithColor:(UIColor *)color atFrame:(CGRect)frame;
- (void)setBordersFromBordersStyle;
- (void)recursiveEndEditing:(BOOL)endEditing;
- (void)shake;
- (UIImage *)takeSnapshot;
- (id)parentViewController;
- (void)pushBack;
- (void)pushFront;

@end
