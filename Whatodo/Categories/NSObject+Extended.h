//
//  NSObject+Extended.h
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface NSObject (Extended)

- (NSString *)className;
- (NSString *)typeFromProperty:(objc_property_t)property;
- (void)populateWithJsonDictionary:(NSDictionary *)jsonDictionary;
- (id)replaceNSNullByNil;
- (NSDictionary *)properties;
- (void)encodeAutoWithCoder:(NSCoder *)aCoder;
- (void)decodeAutoWithCoder:(NSCoder *)aDecoder;
- (void)encodeAutoWithCoder:(NSCoder *)aCoder class:(Class)classObject;
- (void)decodeAutoWithCoder:(NSCoder *)aDecoder class:(Class)classObject;

@end
