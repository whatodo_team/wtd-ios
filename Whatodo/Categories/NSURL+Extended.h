//
//  NSURL+Extended.h
//
//  Created by Gaëtan RATTIN on 29/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Extended)

- (NSString *)getValueForParameter:(NSString *)parameter;
- (NSDictionary *)getParametersDictionary;

@end
