//
//  NSDate+Extended.h
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extended)

- (NSDate *)dateWithoutTime;
- (NSDate *)dateWithoutYear;
- (NSDate *)dateByAddingDays:(NSInteger) days;
- (NSDate *)dateByAddingMonths:(NSInteger) months;
- (NSDate *)dateByAddingYears:(NSInteger) years;
- (NSDate *)dateByAddingDays:(NSInteger) days months:(NSInteger) months years:(NSInteger) years;
- (NSDate *)monthStartDate;
- (NSUInteger)numberOfDaysInMonth;
- (NSUInteger)weekday;
- (NSInteger)daysSinceDate:(NSDate *) date;
- (NSString *)dateStringWithFormat:(NSString *)format;
- (NSDate *)dateBySettingTime:(NSString *)time;
+ (NSString *)getNowDateString;
+ (NSString *)getDateStringFromDate:(NSDate *)date;
+ (NSString *)getShortDateStringFromDate:(NSDate *)date;
+ (NSString *)getFormattedDateStringFromDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate longStyle:(BOOL)longStyle withYear:(BOOL)withYear;
+ (NSString *)getFormattedTimeStringFromDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate separator:(NSString *)separator;
+ (NSString *)getFormattedDateStringFromFormatedDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate toFormatDate:(NSString *)toFormatDate;
+ (NSString *)getFormattedDateStringFromFormatedDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate toFormatDate:(NSString *)toFormatDate localizedYesterdayString:(NSString *)localizedYesterdayString localizedTodayString:(NSString *)localizedTodayString localizedTomorrowString:(NSString *)localizedTomorrowString;
+ (NSDate *)clampDate:(NSDate *)date toComponents:(NSUInteger)unitFlags;
+ (BOOL)clampAndCompareDate:(NSDate *)date withReferenceDate:(NSDate *)referenceDate;

@end
