//
//  CategoriesHeader.h
//  PhoceisVisionDemo
//
//  Created by Gaëtan RATTIN on 27/01/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#ifndef CategoriesHeader_h
#define CategoriesHeader_h

#import <UIKit/UIKit.h>
#import "MKMapView+Extended.h"
#import "NSDate+Extended.h"
#import "NSDictionary+Extended.h"
#import "NSManagedObjectContext+Extended.h"
#import "NSObject+Extended.h"
#import "NSString+Extended.h"
#import "NSURL+Extended.h"
#import "UIColor+Extended.h"
#import "UIAlertController+Extended.h"
#import "UIAlertView+Blocks.h"
#import "UIButton+Extended.h"
#import "UIImage+Extended.h"
#import "UIStoryboard+Extended.h"
#import "UIView+AutoLayout.h"
#import "UIView+Extended.h"
#import "UIViewController+Extended.h"
#import "UIWindow+Extended.h"

#endif /* CategoriesHeader_h */
