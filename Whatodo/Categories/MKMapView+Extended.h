//
//  MKMapView+Extended.h
//  TAO
//
//  Created by Gaëtan RATTIN on 03/03/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (Extended)

- (void)resetMapViewExceptUserLocation;
- (void)zoomToFitAnnotations;

@end
