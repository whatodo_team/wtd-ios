//
//  MKMapView+Extended.m
//  TAO
//
//  Created by Gaëtan RATTIN on 03/03/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "MKMapView+Extended.h"

@implementation MKMapView (Extended)

//Reset de la map view...
- (void)resetMapViewExceptUserLocation{
    
    //Reset des overlays au besoin...
    [self removeOverlays:self.overlays];
    
    //Reset des annotations (pin stores)...
    id userLocation = self.userLocation;
    NSMutableArray *allAnnotations = [[NSMutableArray alloc] initWithArray:[self annotations]];
    if (userLocation != nil) {
        [allAnnotations removeObject:userLocation];
    }
    [self removeAnnotations:allAnnotations];
    
    //Refresh...
    [self setNeedsDisplay];
    
}


//Centre sur les points d'une carte donnée...
- (void)zoomToFitAnnotations{
    
    if ([self.annotations count] > 0){
        CLLocationCoordinate2D topLeftCoord = CLLocationCoordinate2DMake(-90.0f, 180.0f);
        CLLocationCoordinate2D bottomRightCoord = CLLocationCoordinate2DMake(90.0f, -180.0f);
        for(id<MKAnnotation> annotation in self.annotations) {
            topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
            topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
            bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
            bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
        }
        MKCoordinateRegion region;
        region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
        region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
        region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.8;
        region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.8;
        region = [self regionThatFits:region];
        [self setRegion:region animated:YES];
    }
    
}

@end
