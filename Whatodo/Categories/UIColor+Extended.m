//
//  UIColor+Extended.m
//  TAO
//
//  Created by Gaëtan RATTIN on 07/03/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "UIColor+Extended.h"

@implementation UIColor (Extended)

//Indique si une couleur est à peu près égale à une autre (même range)...
- (BOOL)isEqualToColor:(UIColor *)color withTolerance:(CGFloat)tolerance {
    
    CGFloat r1, g1, b1, a1, r2, g2, b2, a2;
    [self getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    [color getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
    return fabs(r1 - r2) <= tolerance && fabs(g1 - g2) <= tolerance && fabs(b1 - b2) <= tolerance && fabs(a1 - a2) <= tolerance;

}

+ (UIColor *)wtdPrimaryColor{
    return [UIColor colorWithRed:0 green:0.31 blue:0.49 alpha:1];
}
+ (UIColor *)wtdSecondaryColor{
    return [UIColor colorWithRed:0.54 green:0.77 blue:0.25 alpha:1];
}
@end
