//
//  UIWindow+Extended.m
//  TAO
//
//  Created by Gaëtan RATTIN on 29/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "UIWindow+Extended.h"

@implementation UIWindow (Extended)

- (id)getVisibleViewController{
    UIViewController *rootViewController = self.rootViewController;
    return [UIWindow getVisibleViewControllerFromViewController:rootViewController];
}

+ (id)getApplicationVisibleViewController{

    UIWindow *window = [UIApplication sharedApplication].windows.firstObject;
    if (window){
        return [window getVisibleViewController];
    }
    return nil;
    
}

+ (id)getVisibleViewControllerFromViewController:(UIViewController *)vc{
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [UIWindow getVisibleViewControllerFromViewController:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [UIWindow getVisibleViewControllerFromViewController:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [UIWindow getVisibleViewControllerFromViewController:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

@end
