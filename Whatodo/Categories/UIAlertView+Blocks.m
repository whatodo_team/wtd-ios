//
//  UIAlertView+Blocks.m
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "UIAlertView+Blocks.h"
#import <objc/runtime.h>

static const NSString * kUIAlertViewClickHandlerKey = @"kUIAlertViewClickHandlerKey";

@implementation UIAlertView (Blocks)
+ (void)showWithTitle:(NSString *)title message:(NSString *)message clickHandler:(void (^)(UIAlertView *, NSInteger))clickHandler cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... {

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    
    if (otherButtonTitles != nil) {
        [alert addButtonWithTitle:otherButtonTitles];
        va_list args;
        va_start(args, otherButtonTitles);
        NSString * title = nil;
        while((title = va_arg(args, NSString*))) {
            [alert addButtonWithTitle:title];
        }
        va_end(args);
    }
    alert.delegate = alert;
    objc_setAssociatedObject(alert, (__bridge const void *)(kUIAlertViewClickHandlerKey), clickHandler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [alert show];
}
+ (void)showWithTitle:(NSString *)title message:(NSString *)message clickHandler:(void (^)(UIAlertView *, NSInteger))clickHandler cancelButtonTitle:(NSString *)cancelButtonTitle {
    [self showWithTitle:title message:message clickHandler:clickHandler cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
}
- (void)showWithClickHandler:(void (^)(UIAlertView *, NSInteger))clickHandler {
    self.delegate = self;
    objc_setAssociatedObject(self, (__bridge const void *)(kUIAlertViewClickHandlerKey), clickHandler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self show];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    void(^clickHandler)(UIAlertView *, NSInteger) = objc_getAssociatedObject(self, (__bridge const void *)(kUIAlertViewClickHandlerKey));
    if (clickHandler) {
        clickHandler(alertView, buttonIndex);
    }
}
@end
