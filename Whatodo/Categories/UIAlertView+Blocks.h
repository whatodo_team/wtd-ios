//
//  UIAlertView+Blocks.h
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Blocks) <UIAlertViewDelegate>
+ (void)showWithTitle:(NSString *)title message:(NSString *)message clickHandler:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))clickHandler cancelButtonTitle:(NSString *)cancelButtonTitle;
+ (void)showWithTitle:(NSString *)title message:(NSString *)message clickHandler:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))clickHandler cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;
- (void)showWithClickHandler:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))clickHandler;

@end
