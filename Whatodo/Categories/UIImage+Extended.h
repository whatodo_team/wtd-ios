//
//  UIImage+Extended.h
//
//  Created by Gaëtan RATTIN on 01/12/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extended)
- (UIImage *)imageMaskWithColor:(UIColor *)maskColor shadowOffset:(CGPoint)shadow;
- (UIImage *)getRoundedImageOnReferenceView:(UIImageView*)imageView;
- (UIImage *)fixOrientation;
- (UIImage *)imageWithGaussianBlur;
+ (UIImage *)imageWithURLString:(NSString *)urlString defaultImage:(UIImage *)defaultImage;
+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color;
+ (UIImage *)changeBlackBackgroundImage:(UIImage*)image toHexaColor:(uint)hexaColor;
+ (UIImage *)croppedImage:(UIImage *)image cropRect:(CGRect)cropRect;
+ (UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height;
+ (UIImage *)screenshotFromView:(UIView *)aView;
+ (UIImage *)image:(UIImage *)image withColor:(UIColor *)color;
@end
