//
//  UIButton+Extended.h
//
//  Created by Gaëtan RATTIN on 26/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extended)

- (void)centerImageAndText;
- (void)centerImageAndTextWithPadding:(float)padding;

@end
