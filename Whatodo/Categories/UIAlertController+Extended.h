//
//  UIAlertController+Extended.h
//
//  Created by Gaëtan RATTIN on 04/03/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^OnAlertAction)(UIAlertController *alertController);

@interface UIAlertController (Extended)

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction;

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction;

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction tintColor:(UIColor *)tintColor;

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction;

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction tintColor:(UIColor *)tintColor;


@end
