//
//  UIViewController+Extended.h
//
//  Created by Gaëtan RATTIN on 17/06/2015.
//  Copyright (c) 2015 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extended)

- (BOOL)isModal;
- (void)pushBackAnimation;
- (void)pushFrontAnimation;

@end
