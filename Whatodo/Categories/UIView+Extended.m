//
//  UIView+Extended.m
//
//  Created by Gaëtan RATTIN on 08/11/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "UIView+Extended.h"

@implementation UIView (Extended)

@dynamic bordersStyle;
//@dynamic layerMaskIsSet;

//Met la vue en cercle...
- (void)doCircle {
    CALayer *lyr = self.layer;
    //lyr.masksToBounds = YES;
    lyr.cornerRadius = self.frame.size.width / 2;
}

//Mets la vue en cercle avec une couleur de bordure. Une ombre peut être appliquée...
- (void)doCircleWithBorderColor:(UIColor *)color width:(CGFloat)width withShadow:(BOOL)withShadow shadowColor:(UIColor *)shadowColor{
    CALayer *lyr = self.layer;
    [self doCircle];
    lyr.borderWidth = width;
    lyr.borderColor = color.CGColor;
    lyr.masksToBounds = YES;
    self.clipsToBounds = YES;
    if (withShadow && shadowColor){
        lyr.shadowColor = shadowColor.CGColor;
        lyr.shadowOffset = CGSizeMake(3.0f, 3.0f);
        lyr.shadowOpacity = 0.9f;
        lyr.shadowRadius = 5.0f;
        lyr.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.frame.size.width / 2.0f].CGPath;
    }
}

//Crée une vue arrondie aux bords avec une ombre...
- (void)doRoundedShadowedView{
    self.layer.cornerRadius = 3.0f;
    self.layer.shadowOpacity = 0.8;
    self.layer.shadowRadius = 2.0;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.layer.bounds].CGPath;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

//Créé un rectangle à bords arrondis avec une couleur de bordure...
- (void)doRoundedBordersWithBorderColor:(UIColor *)color width:(CGFloat)width cornerRadius:(CGFloat)corderRadius{
    CALayer *lyr = self.layer;
    lyr.borderWidth = width;
    lyr.borderColor = [color CGColor];
    lyr.cornerRadius = 3.0f;
    lyr.shouldRasterize = YES;
    lyr.rasterizationScale = [UIScreen mainScreen].scale;
}

//Crée un rectangle avec une couleur de bordure...
- (void)doRectangleWithBorderColor:(UIColor *)color width:(CGFloat)width{
    CALayer *lyr = self.layer;
    lyr.borderWidth = width;
    lyr.borderColor = [color CGColor];
    lyr.shouldRasterize = YES;
    lyr.rasterizationScale = [UIScreen mainScreen].scale;
}

//Crée une ombre soft avec une couleur donnée...
- (void)doSoftShadowWithColor:(UIColor *)color shadowRadius:(CGFloat)shadowRadius{
    
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.layer.shadowOpacity = 0.8f;
    self.layer.shadowRadius = shadowRadius;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
}

//Crée une ombre soft avec une couleur donnée...
- (void)doSoftShadowUsingCGPathWithColor:(UIColor *)color shadowRadius:(CGFloat)shadowRadius{
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.layer.cornerRadius];
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.layer.shadowOpacity = 0.8f;
    self.layer.shadowRadius = shadowRadius;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.shadowPath = [path CGPath];
    
}
//Ajoute une bordure avec couleur...
- (void)addBorderWithColor:(UIColor *)color borderWidth:(CGFloat)borderWidth onTop:(BOOL)onTop onBottom:(BOOL)onBottom{
    
    if (onTop){
        [self addBorderWithColor:color atFrame:CGRectMake(0, 0, self.bounds.size.width, borderWidth)];
    }
    if (onBottom){
        [self addBorderWithColor:color atFrame:CGRectMake(0, self.bounds.size.height-borderWidth, self.bounds.size.width, borderWidth)];
    }
    
}

- (void)addBorderWithColor:(UIColor *)color atFrame:(CGRect)frame{
    UIView *borderView = [[UIView alloc] initWithFrame:frame];
    borderView.backgroundColor = color;
    [self addSubview:borderView];
}

//Bordures...
- (void)setBordersStyle:(UIViewBordersStyle)bordersStyle{
    //if (bordersStyle!=self.bordersStyle) self.layerMaskIsSet = NO;
    NSNumber *value = [[NSNumber alloc] initWithInt:bordersStyle];
    objc_setAssociatedObject(self, (__bridge const void*)(@"BORDERSSTYLE"), value, OBJC_ASSOCIATION_ASSIGN);
    [self setBordersFromBordersStyle];
}

- (UIViewBordersStyle)bordersStyle{
    NSNumber *value = objc_getAssociatedObject(self, (__bridge const void*)@"BORDERSSTYLE");
    return (UIViewBordersStyle)[value integerValue];
}

/*- (void)setBounds:(CGRect)value{
 objc_setAssociatedObject(self, @selector(bounds), [NSValue valueWithCGRect:value], OBJC_ASSOCIATION_COPY_NONATOMIC);
 NSLog(@"%@", self.bounds);
 [self setBordersFromBordersStyle];
 }
 - (CGRect)bounds{
 NSValue *value = objc_getAssociatedObject(self, @selector(bounds));
 NSLog(@"%@", value);
 return [value CGRectValue];
 }*/

/*- (void)setLayerMaskIsSet:(BOOL)layerMaskIsSet {
 NSNumber *value = @(layerMaskIsSet);
 objc_setAssociatedObject(self, (__bridge const void*)(@"LAYERMASKISSET"), value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
 }
 
 - (BOOL)layerMaskIsSet{
 NSNumber *value = objc_getAssociatedObject(self, (__bridge const void*)@"LAYERMASKISSET");
 return [value boolValue];
 }*/

- (void)setBordersFromBordersStyle{
    
    if (self.bordersStyle == UIViewBordersStyle_NoBorders) return;
    
    UIRectCorner corners = UIRectCornerAllCorners;
    CGFloat radius = 3.0f;
    switch (self.bordersStyle) {
        case UIViewBordersStyle_RoundedBorders:
            break;
            
        case UIViewBordersStyle_RoundedBordersOnTop:
            corners = UIRectCornerTopLeft | UIRectCornerTopRight;
            break;
            
        case UIViewBordersStyle_RoundedBordersOnBottom:
            corners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            break;
            
        default:
            radius = 0.0f;
            break;
    }
    //if (!self.layerMaskIsSet){
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    //self.layerMaskIsSet = YES;
    //}
    self.clipsToBounds = YES;
    [self setNeedsDisplay];
    
}

//Recursive end editing...
- (void)recursiveEndEditing:(BOOL)endEditing{
    if ([self respondsToSelector:@selector(endEditing:)]){
        [self endEditing:endEditing];
        if (self.superview){
            [self.superview recursiveEndEditing:endEditing];
        }
    }
}

//Animation de type shake...
- (void)shake{
    const int reset = 8;
    const int maxShakes = 8;
    static int shakes = 0;
    static int translate = reset;
    [UIView animateWithDuration:0.09f - (shakes * 0.01f) delay:0.01f options:(enum UIViewAnimationOptions)UIViewAnimationCurveEaseInOut animations:^{
        self.transform = CGAffineTransformMakeTranslation(translate, 0.0f);
    } completion:^(BOOL finished){
        if(shakes < maxShakes){
            shakes++;
            if (translate>0) translate--;
            translate *= -1;
            [self shake];
        }
        else {
            self.transform = CGAffineTransformIdentity;
            shakes = 0;
            translate = reset;
            return;
        }
    }];
}

//Renvoie un snapshot...
-(UIImage *)takeSnapshot{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [self.layer renderInContext:ctx];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshot;
}

//Renvoie le parent view controller lié à la view...
- (id)parentViewController {
    UIResponder *responder = self;
    while ([responder isKindOfClass:[UIView class]])
        responder = [responder nextResponder];
    return responder;
}

//Push back...
- (void)pushBack{
    CABasicAnimation* scaleDown = [CABasicAnimation animationWithKeyPath:@"transform"];
    scaleDown.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.85f, 0.85f, 0.85f)];
    scaleDown.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleDown.removedOnCompletion = YES;
    
    CABasicAnimation* opacity = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacity.fromValue = [NSNumber numberWithFloat:1.0f];
    opacity.toValue = [NSNumber numberWithFloat:0.85f];
    opacity.removedOnCompletion = YES;
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.duration = 0.3f;
    group.animations = [NSArray arrayWithObjects:scaleDown, opacity, nil];
    
    [self.layer addAnimation:group forKey:nil];
    self.layer.transform = (CATransform3DMakeScale(0.85f, 0.85f, 0.85f));
    self.layer.opacity = 0.85f;
}

//Push front...
- (void)pushFront{
    CABasicAnimation* scaleUp = [CABasicAnimation animationWithKeyPath:@"transform"];
    scaleUp.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleUp.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.85f, 0.85f, 0.85f)];
    scaleUp.removedOnCompletion = YES;
    
    CABasicAnimation* opacity = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacity.fromValue = [NSNumber numberWithFloat:0.85f];
    opacity.toValue = [NSNumber numberWithFloat:1.0f];
    opacity.removedOnCompletion = YES;
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.duration = 0.3f;
    group.animations = [NSArray arrayWithObjects:scaleUp, opacity, nil];
    
    [self.layer addAnimation:group forKey:nil];
    self.layer.transform = CATransform3DIdentity;
    self.layer.opacity = 1.0f;
}


@end
