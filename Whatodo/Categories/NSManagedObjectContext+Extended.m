//
//  NSManagedObjectContext+Extended.m
//
//  Created by Gaëtan RATTIN on 08/12/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "NSManagedObjectContext+Extended.h"

@implementation NSManagedObjectContext (Extended)

-(id)objectWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity {
    return [self objectWithPredicate:predicate forEntity:entity includesPropertyValues:YES];
}

-(id)objectWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity includesPropertyValues:(BOOL)includesPropertyValues {
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entity];
    [request setPredicate:predicate];
    request.fetchLimit = 1;
    [request setIncludesPropertyValues:includesPropertyValues];
    NSArray *results = [self executeFetchRequest:request error:NULL];
    if([results count] < 1)
        return nil;
    NSManagedObject *obj = results[0];
    return obj;
}

-(id)objectWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName {
    return  [self objectWithPredicate:predicate forEntityName:entityName includesPropertyValues:YES];
}

-(id)objectWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName includesPropertyValues:(BOOL)includesPropertyValues {
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    [request setPredicate:predicate];
    request.fetchLimit = 1;
    [request setIncludesPropertyValues:includesPropertyValues];
    NSArray *results = [self executeFetchRequest:request error:NULL];
    if([results count] < 1)
        return nil;
    NSManagedObject *obj = results[0];
    return obj;
}

-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity {
    return [self objectsWithPredicate:predicate forEntity:entity includesPropertyValues:YES];
}

-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity includesPropertyValues:(BOOL)includesPropertyValues {
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entity];
    [request setPredicate:predicate];
    [request setIncludesPropertyValues:includesPropertyValues];
    NSArray *results = [self executeFetchRequest:request error:NULL];
    return results;
}

-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName {
    return [self objectsWithPredicate:predicate forEntityName:entityName includesPropertyValues:YES];
}

-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName includesPropertyValues:(BOOL)includesPropertyValues {
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    [request setPredicate:predicate];
    [request setIncludesPropertyValues:includesPropertyValues];
    [request setReturnsObjectsAsFaults:NO];
    NSError *error = nil;
    NSArray *results = [self executeFetchRequest:request error:&error];
    return results;
}

-(NSUInteger)countWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity {
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entity];
    [request setPredicate:predicate];
    NSUInteger result = [self countForFetchRequest:request error:NULL];
    return result;
}

-(NSUInteger)countWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName {
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSUInteger result = [self countForFetchRequest:request error:&error];
    return result;
}

@end
