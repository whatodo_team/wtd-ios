//
//  NSURL+Extended.m
//
//  Created by Gaëtan RATTIN on 29/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "NSURL+Extended.h"

@implementation NSURL (Extended)

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques...
//--------------------------------------------------------------------------------------------------

//Récupère une valeur pour un paramètre donné dans l'url...
- (NSString *)getValueForParameter:(NSString *)parameter{
    NSString *result;
    NSString *requestAbsoluteString = [self absoluteString];
    NSString *pattern = [NSString stringWithFormat:@"(:?\?|&)(:?%@)(:?=)([^&]*)",[parameter stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSRegularExpression *regEx = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    if( [regEx numberOfMatchesInString:requestAbsoluteString options:0 range:NSMakeRange(0, requestAbsoluteString.length)]>0){
        NSTextCheckingResult *textCheckingResult = [regEx firstMatchInString:requestAbsoluteString options:0 range:NSMakeRange(0, requestAbsoluteString.length)];
        NSRange matchRange = [textCheckingResult rangeAtIndex:4];
        result = [[requestAbsoluteString substringWithRange:matchRange] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    return result;
}

//Recupère le dictionnaire de paramètres de l'url...
- (NSDictionary *)getParametersDictionary{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    NSString *query = [self query];
    NSArray *parameters = [query componentsSeparatedByString:@"&"];
    for (NSString *parameterAndValue in parameters){
        NSMutableArray *datas = [NSMutableArray arrayWithArray:[parameterAndValue componentsSeparatedByString:@"="]];
        if (datas.count == 2){
            NSRegularExpression *regEx = [NSRegularExpression regularExpressionWithPattern:@"(\\[[0-9]*\\])" options:NSRegularExpressionCaseInsensitive error:nil];
            NSString *parameter = [regEx stringByReplacingMatchesInString:[datas objectAtIndex:0] options:0 range:NSMakeRange(0, [[datas objectAtIndex:0] length]) withTemplate:@""];
            NSString *value = [datas objectAtIndex:1];
            if ([dictionary objectForKey:parameter]!=nil){
                if ([[dictionary objectForKey:parameter] isKindOfClass:[NSArray class]]){
                    [[dictionary objectForKey:parameter] addObject:value];
                }
                else{
                    NSString *oldValue = [dictionary objectForKey:parameter];
                    NSMutableArray *array = [NSMutableArray arrayWithObject:oldValue];
                    [dictionary setObject:array forKey:parameter];
                };
            }
            else{
                [dictionary setObject:value forKey:parameter];
            };
        }
    };
    return [NSDictionary dictionaryWithDictionary:dictionary];
}


@end
