//
//  NSDictionary+Extended.h
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extended)

- (BOOL)containsKey:(NSString *)key;
- (NSString *)jsonStringWithPrettyPrint:(BOOL)prettyPrint;

@end
