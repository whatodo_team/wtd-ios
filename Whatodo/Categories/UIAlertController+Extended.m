//
//  UIAlertController+Extended.m
//
//  Created by Gaëtan RATTIN on 04/03/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "UIAlertController+Extended.h"

@implementation UIAlertController (Extended)


//--------------------------------------------------------------------------------------------------
#pragma mark - Instanciation...
//--------------------------------------------------------------------------------------------------
+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction{

    return [UIAlertController alertControllerWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:nil onOKAction:onOKAction onCancelAction:nil];
    
}

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction{

    //Création d'un alert controller...
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    //Création du bouton OK...
    __weak UIAlertController *weakAlertController;
    void (^dissmissTextFields)() = ^(){
        if (alertController.textFields){
            for (UITextField *textField in alertController.textFields){
                [textField resignFirstResponder];
            }
        }
    };
    
    if (okButtonTitle){
        [alertController addAction:[UIAlertAction actionWithTitle:okButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            dissmissTextFields();
            if (onOKAction) onOKAction(alertController);
            [weakAlertController dismissViewControllerAnimated:YES completion:nil];
        }]];
    }
    
    //Création du bouton Cancel...
    if (cancelButtonTitle){
        [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            dissmissTextFields();
            if (onCancelAction) onCancelAction(alertController);
            [weakAlertController dismissViewControllerAnimated:YES completion:nil];
        }]];
    }
    
    return alertController;
    
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Instantiation et présentation de suite...
//--------------------------------------------------------------------------------------------------
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction{

    [UIAlertController showWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:nil onOKAction:onOKAction onCancelAction:nil tintColor:nil];

}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction tintColor:(UIColor *)tintColor{

    [UIAlertController showWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:nil onOKAction:onOKAction onCancelAction:nil tintColor:tintColor];

}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction{

    [UIAlertController showWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:cancelButtonTitle onOKAction:onOKAction onCancelAction:onCancelAction tintColor:nil];

}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction tintColor:(UIColor *)tintColor{

    //Création de l'alert controller...
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:cancelButtonTitle onOKAction:onOKAction onCancelAction:onCancelAction];
    
    //Présentation...
    UIViewController *vc = [UIWindow getApplicationVisibleViewController];
    if (vc){
        //Présentation...
        [vc presentViewController:alertController animated:YES completion:nil];
        //Tint color -> Bug Apple? Pour que ce soit optimal, il faut mettre la tint après le present (depuis iOS 9.0)...
        
    }
    
}


@end
