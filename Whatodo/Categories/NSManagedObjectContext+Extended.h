//
//  NSManagedObjectContext+Extended.h
//
//  Created by Gaëtan RATTIN on 08/12/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Extended)

-(id)objectWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName;
-(id)objectWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName includesPropertyValues:(BOOL)includesPropertyValues;
-(id)objectWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity;
-(id)objectWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity includesPropertyValues:(BOOL)includesPropertyValues;
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName;
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName includesPropertyValues:(BOOL)includesPropertyValues;
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity;
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity includesPropertyValues:(BOOL)includesPropertyValues;
-(NSUInteger)countWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName;
-(NSUInteger)countWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity;

@end
