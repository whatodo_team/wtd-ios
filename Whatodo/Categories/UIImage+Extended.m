//
//  UIImage+Extended.m
//
//  Created by Gaëtan RATTIN on 01/12/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "UIImage+Extended.h"

@implementation UIImage (Extended)

//Crée un masque d'image avec une couleur donnée...
//Possibilité d'indiquer l'offset de l'ombre...
- (UIImage *)imageMaskWithColor:(UIColor *)maskColor shadowOffset:(CGPoint)shadow{
    
    CGFloat shadowOpacity = 0.54f;
    BOOL hasShadow = !(shadow.x == 0.0f && shadow.y == 0.0f);
    
    CGRect imageRect = CGRectMake(0.0f, 0.0f, self.size.width, self.size.height);
    CGRect shadowRect = CGRectMake(shadow.x, shadow.y, self.size.width, self.size.height);
    
    CGRect newRect = hasShadow ? CGRectUnion(imageRect, shadowRect) : imageRect;
    
    UIGraphicsBeginImageContextWithOptions(newRect.size, NO, self.scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextScaleCTM(ctx, 1.0f, -1.0f);
    CGContextTranslateCTM(ctx, 0.0f, -(newRect.size.height));
    CGContextSaveGState(ctx);
    
    if(hasShadow) {
        CGContextClipToMask(ctx, shadowRect, self.CGImage);
        CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:0 alpha:shadowOpacity].CGColor);
        CGContextFillRect(ctx, shadowRect);
    }
    
    CGContextRestoreGState(ctx);
    CGContextClipToMask(ctx, imageRect, self.CGImage);
    CGContextSetFillColorWithColor(ctx, maskColor.CGColor);
    CGContextFillRect(ctx, imageRect);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

- (UIImage *)getRoundedImageOnReferenceView:(UIImageView*)imageView{
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [[UIBezierPath bezierPathWithRoundedRect:imageView.bounds cornerRadius:imageView.bounds.size.width/2.0f] addClip];
    [self drawInRect:imageView.bounds];
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}

- (UIImage *)fixOrientation{
    
    if (self.imageOrientation == UIImageOrientationUp) {
        return self;
    }
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.height, self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage);
            break;
    }
    
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    
    return img;
}

- (UIImage *)imageWithGaussianBlur{
    
    //Initialisation...
    float weight[5] = {0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162};
    
    //Blur horizontally...
    UIGraphicsBeginImageContext(self.size);
    [self drawInRect:CGRectMake(0, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int x = 1; x < 5; ++x) {
        [self drawInRect:CGRectMake(x, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
        [self drawInRect:CGRectMake(-x, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
    }
    UIImage *horizBlurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //Blur vertically...
    UIGraphicsBeginImageContext(self.size);
    [horizBlurredImage drawInRect:CGRectMake(0, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int y = 1; y < 5; ++y) {
        [horizBlurredImage drawInRect:CGRectMake(0, y, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
        [horizBlurredImage drawInRect:CGRectMake(0, -y, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
    }
    UIImage *blurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return blurredImage;
    
}

//Charge une image en fonction d'une URL, si aucun retour on met l'image par défaut...
+ (UIImage *)imageWithURLString:(NSString *)urlString defaultImage:(UIImage *)defaultImage{
    
    NSURL *url = [NSURL URLWithString:[urlString getFormattedURL]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data){
        return [[UIImage alloc] initWithData:data];
    }
    else {
        return defaultImage;
    }
    
}

//Remplit une image avec une couleur donné...
+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color{
    
    
    UIGraphicsBeginImageContextWithOptions(source.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [color setFill];
    
    CGContextTranslateCTM(context, 0, source.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
    CGContextDrawImage(context, rect, source.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return coloredImg;
    
}

+(UIImage*)changeBlackBackgroundImage:(UIImage*)image toHexaColor:(uint)hexaColor
{
    CGContextRef ctx;
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    UInt32 * rawData;
    rawData = (UInt32 *) calloc(height * width, sizeof(UInt32));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
#define Mask8(x) ( (x) & 0xFF )
#define R(x) ( Mask8(x) )
#define G(x) ( Mask8(x >> 8 ) )
#define B(x) ( Mask8(x >> 16) )
#define A(x) ( Mask8(x >> 24) )
    
    
    UInt32 * currentPixel = rawData;
    for (NSUInteger j = 0; j < height; j++) {
        for (NSUInteger i = 0; i < width; i++) {
            UInt32 color = *currentPixel;
            if(R(color) == 0 && G(color) == 0 && B(color) == 0 && A(color) > 10)
            {
                *currentPixel =  hexaColor |  (color&0xFF000000);
            }
            currentPixel++;
        }
    }
    
    
#undef R
#undef G
#undef B
#undef A
    
    ctx = CGBitmapContextCreate(rawData,
                                CGImageGetWidth( imageRef ),
                                CGImageGetHeight( imageRef ),
                                8,
                                CGImageGetBytesPerRow( imageRef ),
                                CGImageGetColorSpace( imageRef ),
                                kCGImageAlphaPremultipliedLast |kCGBitmapByteOrder32Big );
    
    imageRef = CGBitmapContextCreateImage (ctx);
    UIImage* rawImage = [UIImage imageWithCGImage:imageRef];
    
    CGContextRelease(ctx);
    free(rawData);
    
    return rawImage;
}

+ (UIImage *)croppedImage:(UIImage *)image cropRect:(CGRect)cropRect{
    
    CGImageRef croppedCGImage = CGImageCreateWithImageInRect(image.CGImage, CGRectMake(0, 0, MIN(image.size.width, image.size.height), MIN(image.size.width, image.size.height)));
    UIImage *croppedImage = [UIImage imageWithCGImage:croppedCGImage scale:1.0f orientation:image.imageOrientation];
    CGImageRelease(croppedCGImage);
    croppedImage = [croppedImage fixOrientation];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cropRect cornerRadius:50.0f];
    CGFloat scale;
    if (croppedImage.size.height > croppedImage.size.width) {
        scale = croppedImage.size.height / CGRectGetHeight(maskPath.bounds);
    } else {
        scale = croppedImage.size.width / CGRectGetWidth(maskPath.bounds);
    }
    [maskPath applyTransform:CGAffineTransformMakeScale(scale, scale)];
    CGPoint translation = CGPointMake(-CGRectGetMinX(maskPath.bounds), -CGRectGetMinY(maskPath.bounds));
    [maskPath applyTransform:CGAffineTransformMakeTranslation(translation.x, translation.y)];
    UIGraphicsBeginImageContext(maskPath.bounds.size);
    [maskPath addClip];
    CGPoint point = CGPointMake((CGRectGetWidth(maskPath.bounds) - croppedImage.size.width) * 0.5f,
                                (CGRectGetHeight(maskPath.bounds) - croppedImage.size.height) * 0.5f);
    [croppedImage drawAtPoint:point];
    croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    croppedImage = [UIImage imageWithCGImage:croppedImage.CGImage];
    return croppedImage;
    
}

+ (UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height {
    
    CGSize size = image.size;
    
    CGSize viewsize = CGSizeMake(width, height);
    CGFloat scalex = viewsize.width / size.width;
    CGFloat scaley = viewsize.height / size.height;
    CGFloat scale = MAX(scalex, scaley);
    UIGraphicsBeginImageContextWithOptions(viewsize, NO, [[UIScreen mainScreen] scale]);
    
    CGFloat newWidth = size.width * scale;
    CGFloat newHeight = size.height * scale;
    
    float dwidth = ((viewsize.width - newWidth) / 2.0f);
    float dheight = ((viewsize.height - newHeight) / 2.0f);
    
    CGRect rect = CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
    [image drawInRect:rect];
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
    
    
}

+ (UIImage *)screenshotFromView:(UIView *)aView{
    
    UIGraphicsBeginImageContextWithOptions(aView.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}

//Rend l'image de la couleur passée en paramètre...
+ (UIImage *)image:(UIImage *)image withColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage *returnedImage = [UIImage imageWithCGImage:img.CGImage scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationDown];
    return returnedImage;
    
    /*UIGraphicsBeginImageContextWithOptions(image.size, YES, [[UIScreen mainScreen] scale]);
     CGRect contextRect;
     contextRect.origin.x = 0.0f;
     contextRect.origin.y = 0.0f;
     contextRect.size = [image size];
     CGSize itemImageSize = [image size];
     CGPoint itemImagePosition;
     itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
     itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height) );
     UIGraphicsBeginImageContext(contextRect.size);
     CGContextRef c = UIGraphicsGetCurrentContext();
     CGContextBeginTransparencyLayer(c, NULL);
     CGContextScaleCTM(c, 1.0, -1.0);
     CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [image CGImage]);
     const CGFloat* colors = CGColorGetComponents( color.CGColor );
     CGContextSetRGBFillColor(c, colors[0], colors[1], colors[2], .75);
     contextRect.size.height = -contextRect.size.height;
     contextRect.size.height -= 15;
     CGContextFillRect(c, contextRect);
     CGContextEndTransparencyLayer(c);
     UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return img;*/
    
}


@end
