//
//  UIStoryboard+Extended.m
//
//  Created by Gaëtan RATTIN on 02/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "UIStoryboard+Extended.h"

@implementation UIStoryboard (Extended)

+ (UIViewController *)instantiateInitialControllerWithStoryboardName:(NSString *)name{
    return [[UIStoryboard storyboardWithName:name bundle:nil] instantiateInitialViewController];
}

+ (UIViewController*)instantiateViewControllerWithName:(NSString*)viewControllerName fromStoryBoardName:(NSString*)storyBoardName{
    UIStoryboard* storyBoard = [UIStoryboard storyboardWithName: storyBoardName
                                                         bundle: nil];
    return [storyBoard instantiateViewControllerWithIdentifier: viewControllerName];
}

+ (id)getViewControllerWithIdentifier:(NSString *)viewControllerIdentifier inStoryboardWithName:(NSString *)storyboardName{
    if (viewControllerIdentifier && storyboardName){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        if (storyboard){
            return [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
        }
    }
    return nil;
}


@end
