//
//  UIButton+Extended.m
//
//  Created by Gaëtan RATTIN on 26/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import "UIButton+Extended.h"

@implementation UIButton (Extended)

- (void)centerImageAndText{
    const CGFloat kDefaultPadding = 6.0f;
    [self centerImageAndTextWithPadding:kDefaultPadding];
}

- (void)centerImageAndTextWithPadding:(float)padding{
    
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    self.imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight - imageSize.height), (self.frame.size.width - imageSize.width) / 2.0f, 0.0f, (self.frame.size.width - imageSize.width) / 2.0f);
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f, -imageSize.width, -(totalHeight - titleSize.height), 0.0f);
    self.titleLabel.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, 21.0f);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
}

@end
