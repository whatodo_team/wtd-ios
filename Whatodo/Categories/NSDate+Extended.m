//
//  NSDate+Extended.m
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import "NSDate+Extended.h"

@implementation NSDate (Extended)

- (NSDate *)dateWithoutTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                             fromDate:self];
    
    if (components) return [calendar dateFromComponents:components];
    return nil;
}

- (NSDate *)dateWithoutYear{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitMonth
                                                         | NSCalendarUnitDay )
                                               fromDate:self];
    
    if (components) return [calendar dateFromComponents:components];
    return nil;
}

- (NSDate *) dateByAddingDays:(NSInteger) days months:(NSInteger) months years:(NSInteger) years
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.day = days;
    dateComponents.month = months;
    dateComponents.year = years;
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents
                                                         toDate:self
                                                        options:0];
}

- (NSDate *) dateByAddingDays:(NSInteger) days
{
    return [self dateByAddingDays:days months:0 years:0];
}

- (NSDate *) dateByAddingMonths:(NSInteger) months
{
    return [self dateByAddingDays:0 months:months years:0];
}

- (NSDate *) dateByAddingYears:(NSInteger) years
{
    return [self dateByAddingDays:0 months:0 years:years];
}

- (NSDate *) monthStartDate
{
    NSDate *monthStartDate = nil;
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitMonth
                                    startDate:&monthStartDate
                                     interval:NULL
                                      forDate:self];
    
    return monthStartDate;
}

- (NSUInteger) numberOfDaysInMonth
{
    return [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay
                                              inUnit:NSCalendarUnitMonth
                                             forDate:self].length;
}

- (NSUInteger) weekday
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *weekdayComponents = [gregorian components:NSCalendarUnitWeekday fromDate:self];
    
    return [weekdayComponents weekday];
}

- (NSString *) dateStringWithFormat:(NSString *) format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:self];
}

- (NSInteger) daysSinceDate:(NSDate *) date
{
    return [self timeIntervalSinceDate:date] / (60 * 60 * 24);
}

- (NSDate *) dateBySettingTime:(NSString *)time{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *theDate = [dateFormat stringFromDate:self];
    
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@", theDate, time]];
    
}

+ (NSString *)getNowDateString{
    NSDateFormatter *dfNew = [[NSDateFormatter alloc] init];
    [dfNew setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    //[dfNew setDateStyle:NSDateFormatterFullStyle];
    return [dfNew stringFromDate:[NSDate date]];
}

+ (NSString *)getDateStringFromDate:(NSDate *)date{
    NSDateFormatter *dfNew = [[NSDateFormatter alloc] init];
    [dfNew setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    //[dfNew setDateStyle:NSDateFormatterFullStyle];
    return [dfNew stringFromDate:date];
}

+ (NSString *)getShortDateStringFromDate:(NSDate *)date{
    NSDateFormatter *dfNew = [[NSDateFormatter alloc] init];
    [dfNew setDateFormat: @"yyyy-MM-dd"];
    //[dfNew setDateStyle:NSDateFormatterFullStyle];
    return [dfNew stringFromDate:date];
}

+ (NSString *)getFormattedDateStringFromDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate longStyle:(BOOL)longStyle withYear:(BOOL)withYear{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr-FR"];
    [df setLocale:locale];
    [df setDateFormat:fromFormatDate];
    NSDate *fDate = [df dateFromString:date];
    /*NSDateFormatter *dfNew = [[NSDateFormatter alloc] init];
     [dfNew setLocale:locale];
     [dfNew setDateStyle:NSDateFormatterLongStyle];
     NSDate *currentDate = [dfNew dateFromString:[dfNew stringFromDate:[NSDate date]]];
     NSDate *fDateNew = [dfNew dateFromString:[dfNew stringFromDate:fDate]];
     if ([currentDate compare:fDateNew]==NSOrderedSame){
     return @"Aujourd'hui";
     }*/
    if (longStyle && !withYear){
        NSLocale *currentLocale = [NSLocale currentLocale];
        NSString *dateComponents = @"EEEEMMMMd";
        NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:currentLocale];
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:dateFormat];
        return [[dateFormatter stringFromDate:fDate] capitalizedString];
    }
    else if (longStyle){
        [df setDateStyle:NSDateFormatterFullStyle];
    }
    else {
        [df setDateStyle:NSDateFormatterShortStyle];
    }
    return [[df stringFromDate:fDate] capitalizedString];
}

+ (NSString *)getFormattedTimeStringFromDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate separator:(NSString *)separator{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-EN"];
    [df setLocale:locale];
    [df setDateFormat:fromFormatDate];
    NSDate *fDate = [df dateFromString:date];
    [df setDateFormat:@"HH"];
    NSString *returnValue = [df stringFromDate:fDate];
    [df setDateFormat:@"mm"];
    if (!separator){
        separator = @":";
    }
    returnValue = [NSString stringWithFormat:@"%@%@%@", [returnValue replaceNSNullByNil], separator, [[df stringFromDate:fDate] replaceNSNullByNil]];
    return returnValue;
}

+ (NSString *)getFormattedDateStringFromFormatedDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate toFormatDate:(NSString *)toFormatDate{
    if (date || ![date isEqualToString:@""]){
        static NSDateFormatter *df;
        if (df == nil){
            df = [[NSDateFormatter alloc] init];
        }
        [df setDateFormat:fromFormatDate];
        NSDate *fDate = [df dateFromString:date];
        if (!fDate){
            [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
            fDate = [df dateFromString:date];
        }
        [df setDateFormat:toFormatDate?toFormatDate:@"yyyy-MM-dd HH:mm:ss"];
        NSString *returnString = [df stringFromDate:fDate];
        return returnString?[returnString capitalizedString]:nil;
    }
    return nil;
}

+ (NSString *)getFormattedDateStringFromFormatedDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate toFormatDate:(NSString *)toFormatDate localizedYesterdayString:(NSString *)localizedYesterdayString localizedTodayString:(NSString *)localizedTodayString localizedTomorrowString:(NSString *)localizedTomorrowString{
    
    if (date || ![date isEqualToString:@""]){
        
        static NSDateFormatter *df;
        if (df == nil) df = [[NSDateFormatter alloc] init];
        
        [df setDateFormat:fromFormatDate];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr-FR"];
        [df setLocale:locale];
        NSDate *fDate = [df dateFromString:date];
        if (!fDate){
            [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
            fDate = [df dateFromString:date];
        }
        toFormatDate = toFormatDate?:@"yyyy-MM-dd HH:mm:ss";
        BOOL toFormatDateHaveTime = [toFormatDate rangeOfString:@"HH"].location!=NSNotFound;
        [df setDateFormat:toFormatDate];
        
        NSString *formattedDateString = [df stringFromDate:fDate];
        NSString *timeString = toFormatDateHaveTime?[NSDate getFormattedTimeStringFromDateString:date fromFormatDate:fromFormatDate separator:@":"]:nil;
        
        static NSDateFormatter *formatter;
        if (formatter == nil) formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitWeekday;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        if (fDate){
            NSDateComponents *comps = [calendar components:unitFlags fromDate:fDate];
            [comps setHour:0];
            [comps setMinute:0];
            [comps setSecond:0];
            NSDate *suppliedDate = [calendar dateFromComponents:comps];
            int i;
            for (i = -1; i < 7; i++){
                comps = [calendar components:unitFlags fromDate:[NSDate date]];
                [comps setHour:0];
                [comps setMinute:0];
                [comps setSecond:0];
                [comps setDay:[comps day] - i];
                if (comps){
                    NSDate *referenceDate = [calendar dateFromComponents:comps];
                    if ([suppliedDate compare:referenceDate] == NSOrderedSame && i == -1){
                        return !toFormatDateHaveTime?localizedTomorrowString:[NSString stringWithFormat:@"%@ %@", localizedTomorrowString, timeString];
                    }
                    else if ([suppliedDate compare:referenceDate] == NSOrderedSame && i == 0){
                        return !toFormatDateHaveTime?localizedTodayString:[NSString stringWithFormat:@"%@ %@", localizedTodayString, timeString];;
                    }
                    else if ([suppliedDate compare:referenceDate] == NSOrderedSame && i == 1){
                        return !toFormatDateHaveTime?localizedYesterdayString:[NSString stringWithFormat:@"%@ %@", localizedYesterdayString, timeString];
                    }
                }
            }
            return formattedDateString?[formattedDateString capitalizedString]:nil;
        }
    }
    return nil;
    
}

+ (NSDate *)clampDate:(NSDate *)date toComponents:(NSUInteger)unitFlags{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:unitFlags fromDate:date];
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

+ (BOOL)clampAndCompareDate:(NSDate *)date withReferenceDate:(NSDate *)referenceDate{
    NSDate *refDate = [self clampDate:referenceDate toComponents:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay)];
    NSDate *clampedDate = [self clampDate:date toComponents:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay)];
    return [refDate isEqualToDate:clampedDate];
}

+ (NSString *)getDateStringWithTodayOrTomorrowIfNeccessaryFromDateString:(NSString *)dateString formatDate:(NSString *)formatDate localizedTodayDateString:(NSString *)localizedTodayDateString localizedTomorrowDateString:(NSString *)localizedTomorrowDateString;{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr-FR"];
    [df setLocale:locale];
    [df setDateFormat:formatDate];
    NSDate *date = [df dateFromString:dateString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    if (date){
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        [comps setHour:0];
        [comps setMinute:0];
        [comps setSecond:0];
        NSDate *suppliedDate = [calendar dateFromComponents:comps];
        
        int i;
        for (i = -1; i < 7; i++)
        {
            comps = [calendar components:unitFlags fromDate:[NSDate date]];
            [comps setHour:0];
            [comps setMinute:0];
            [comps setSecond:0];
            [comps setDay:[comps day] - i];
            if (comps){
                NSDate *referenceDate = [calendar dateFromComponents:comps];
                if ([suppliedDate compare:referenceDate] == NSOrderedSame && i == -1){
                    return localizedTomorrowDateString;
                }
                else if ([suppliedDate compare:referenceDate] == NSOrderedSame && i == 0){
                    return localizedTodayDateString;
                }
                else if ([suppliedDate compare:referenceDate] == NSOrderedSame){
                    return [self getFormattedDateStringFromDateString:[self getDateStringFromDate:date] fromFormatDate:formatDate longStyle:YES withYear:NO];
                }
            }
        }
    }
    return [self getFormattedDateStringFromDateString:[self getDateStringFromDate:date] fromFormatDate:formatDate longStyle:YES withYear:NO];
    
}


@end
