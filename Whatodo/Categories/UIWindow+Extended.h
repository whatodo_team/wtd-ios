//
//  UIWindow+Extended.h
//  TAO
//
//  Created by Gaëtan RATTIN on 29/02/2016.
//  Copyright © 2016 Gaëtan RATTIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (Extended)

- (id)getVisibleViewController;
+ (id)getApplicationVisibleViewController;
+ (id)getVisibleViewControllerFromViewController:(UIViewController *)vc;


@end
