//
//  NSString+Extended.h
//
//  Created by Gaëtan RATTIN on 27/10/2015.
//  Copyright © 2015 Gaëtan RATTIN. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    STTextDirectionNeutral = 0,
    STTextDirectionLeftToRight,
    STTextDirectionRightToLeft,
} STTextDirection;

@interface NSString (Extended)

- (STTextDirection)getBaseDirection;
- (NSString *)getFormattedURL;
- (BOOL)isNotNilAndNotEmpty;
- (BOOL)isEmpty;
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width;
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height;
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width constrainedToHeight:(CGFloat)height;
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedSize:(CGSize)constrainedSize;
- (BOOL)isNumber;
- (NSInteger)numberOfLinesWithFont:(UIFont *)font constrainedSize:(CGSize)constrainedSize;
+ (NSString *)trimString:(NSString *)string;

@end
