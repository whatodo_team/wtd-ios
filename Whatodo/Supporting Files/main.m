//
//  main.m
//  Whatodo
//
//  Created by Valentin Denis on 22/01/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
