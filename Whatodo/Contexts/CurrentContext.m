//
//  CurrentContext.m
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "CurrentContext.h"

@interface CurrentContext ()<CLLocationManagerDelegate>
@property (nonatomic, copy) OnInitialisationCompletion onInitialisationCompletion;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, copy) RequestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion;

@end

@implementation CurrentContext
//Singleton vers une seule instance CurrentContext...
+ (CurrentContext *)sharedInstance{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

//Initialisation...
- (id)init{
    
    self = [super init];
    if (self){
    }
    return self;
    
}

- (void)initializeCurrentContextWithCompletion:(OnInitialisationCompletion)completion{
    
    //Initialisation des objets...
    _onInitialisationCompletion = completion;
    
    [Firebase defaultConfig].persistenceEnabled = YES;

    // See if Connected event
    Firebase *connectedRef = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@.info/connected", kWTD_Firebase]];
    [connectedRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if([snapshot.value boolValue]) {
            NSLog(@"connected");
        } else {
            NSLog(@"not connected");
        }
    }];
    
    // Fini
    if (_onInitialisationCompletion) _onInitialisationCompletion();

}

//Demande l'authorisation de géolocalisation...
- (void)requestGeolocalisationAuthorizationAndGetCurrentUserLocationWithCompletion:(RequestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion)completion{
    
    //On vérifie d'abord l'état d'autorisation de la géolocalisation...
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    _requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion = completion;
    [_locationManager requestAlwaysAuthorization];
}

//--------------------------------------------------------------------------------------------------
#pragma mark - CLLocationManagerDelegate methods...
//--------------------------------------------------------------------------------------------------
//Changement de status d'autorisation de la géolocalisation...
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if (status != kCLAuthorizationStatusNotDetermined){
        if (_requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion){
            BOOL userAuthorizeLocalisation = (status == kCLAuthorizationStatusAuthorizedAlways ||status == kCLAuthorizationStatusAuthorizedWhenInUse);
            if (!userAuthorizeLocalisation){
                //L'utilisateur ne veut pas être géolocalisé...
                //S'il a voulu, on appelera le block après avoir eu sa géolocalisation...
                _requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion(NO, nil);
                _requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion = nil;
                [_locationManager stopUpdatingLocation];
                _locationManager = nil;
            }
        }
    }
}

//Update de la géolocalisation...
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    BOOL userAuthorizeLocalisation = ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways ||[CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse);
    _requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion(userAuthorizeLocalisation, locations?locations.count>0?locations[0]:nil:nil);
    _requestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion = nil;
    [_locationManager stopUpdatingLocation];
    _locationManager = nil;
    
}

@end
