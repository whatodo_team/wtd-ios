//
//  FacebookContext.h
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^OnInitialisationCompletion)(void);
typedef void(^OnLoginCompletion)(void);

@interface FacebookContext : NSObject
+ (FacebookContext *)sharedInstance;
- (void)initializeFacebookContextWithCompletion:(OnInitialisationCompletion)completion;
- (void)connectWithPermissions:(NSArray *)permissions fromViewController:(UIViewController *)viewController withCompletion:(OnLoginCompletion)completion;
-(UIImage *)getAvatarImage;
@property BOOL isConnectedToFacebook;
@property (nonatomic, strong) FAuthData *userData;

@end
