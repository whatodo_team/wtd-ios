//
//  FirebaseContext.h
//  Whatodo
//
//  Created by Valentin Denis on 25/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FirebaseContext : NSObject
+ (FirebaseContext *)sharedInstance;
-(void)checkIfConnectedWithCallback:(void (^)(BOOL ))callback;
@property (nonatomic, retain) Firebase *baseRef;
@property (nonatomic, retain) Firebase *userRef;
@end
