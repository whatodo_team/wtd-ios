//
//  FacebookContext.m
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "FacebookContext.h"
@interface FacebookContext ()
@property (nonatomic, copy) OnInitialisationCompletion onInitialisationCompletion;
@property (nonatomic, copy) OnLoginCompletion onLoginCompletion;

@end

@implementation FacebookContext
@synthesize isConnectedToFacebook;

//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle
//--------------------------------------------------------------------------------------------------
//Singleton vers une seule instance CurrentContext...
+ (FacebookContext *)sharedInstance{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

//Initialisation...
- (id)init{
    
    self = [super init];
    if (self){
    }
    return self;
    
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Setters / Getters
//--------------------------------------------------------------------------------------------------
-(BOOL)isConnectedToFacebook{
    return isConnectedToFacebook;
}

-(void)setIsConnectedToFacebook:(BOOL)isConnected{
    if(isConnectedToFacebook != isConnected){
        isConnectedToFacebook = isConnected;
    }
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques
//--------------------------------------------------------------------------------------------------
- (void)initializeFacebookContextWithCompletion:(OnInitialisationCompletion)completion{
    
    //Initialisation des objets...
    _onInitialisationCompletion = completion;
    
    [[FirebaseContext sharedInstance].baseRef observeAuthEventWithBlock:^(FAuthData *authData) {
        if (authData) {
            if ([authData.provider isEqualToString:@"facebook"]) {
                [self setIsConnectedToFacebook:YES];
                _userData = authData;
                if (_onInitialisationCompletion) _onInitialisationCompletion();

            }else{
                [self setIsConnectedToFacebook:NO];
            }
            
        } else {
            [self setIsConnectedToFacebook:NO];
        }
        // Fini

    }];

    
}

- (void)connectWithPermissions:(NSArray *)permissions fromViewController:(WTDViewController *)viewController withCompletion:(OnLoginCompletion)completion{
    
    _onLoginCompletion = completion;
    
    Firebase *ref = [FirebaseContext sharedInstance].baseRef;
    FBSDKLoginManager *facebookLogin = [[FBSDKLoginManager alloc] init];
    
    __weak FacebookContext *weakSelf = self;
    [facebookLogin logInWithReadPermissions:permissions fromViewController:viewController
                                    handler:^(FBSDKLoginManagerLoginResult *facebookResult, NSError *facebookError) {
                                        
                                        if (facebookError) {
                                            NSLog(@"Facebook login failed. Error: %@", facebookError);
                                        } else if (facebookResult.isCancelled) {
                                            NSLog(@"Facebook login got cancelled.");
                                        } else {
                                            NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                                            
                                            [ref authWithOAuthProvider:@"facebook" token:accessToken
                                                   withCompletionBlock:^(NSError *error, FAuthData *authData) {
                                                       if (error) {
                                                           NSLog(@"Login failed. %@", error);
                                                       } else {
                                                           NSLog(@"Logged in! %@", authData);
                                                           
                                                           NSLog(@"%@", authData.providerData);
                                                           [self saveUserInFirebaseWithAuthData:authData];
                                                           weakSelf.isConnectedToFacebook = YES;
                                                           weakSelf.userData = authData;
                                                       }
                                                   }];
                                        }
                                    }];
}

-(UIImage *)getAvatarImage{
    UIImage *imageWithURL = [UIImage imageWithURLString:_userData.providerData[@"profileImageURL"] defaultImage:nil];
    return imageWithURL;
}
//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions privées
//--------------------------------------------------------------------------------------------------
// Enregistre l'user dans la db puis completion
-(void)saveUserInFirebaseWithAuthData:(FAuthData *)authData{
    
    [[[FirebaseContext sharedInstance].userRef childByAppendingPath:authData.uid] setValue:authData.providerData withCompletionBlock:^(NSError *error, Firebase *ref) {
        if (_onLoginCompletion) _onLoginCompletion();
    }];
}

@end
