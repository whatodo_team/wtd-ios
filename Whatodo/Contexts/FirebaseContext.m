//
//  FirebaseContext.m
//  Whatodo
//
//  Created by Valentin Denis on 25/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "FirebaseContext.h"

@implementation FirebaseContext
//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle
//--------------------------------------------------------------------------------------------------
//Singleton vers une seule instance FirebaseContext...
+ (FirebaseContext *)sharedInstance{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

//Initialisation...
- (id)init{
    
    self = [super init];
    if (self){
        _baseRef = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@",kWTD_Firebase]];
        _userRef = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@%@",kWTD_Firebase, kWTD_Firebase_Users]];

    }
    return self;
    
}
//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques
//--------------------------------------------------------------------------------------------------
-(void)checkIfConnectedWithCallback:(void (^)(BOOL ))callback {
    
    [_baseRef observeAuthEventWithBlock:^(FAuthData *authData) {
        if (authData) {
            // user authenticated, go to home
            callback(YES);
            
        } else {
            callback(NO);
        }
    }];
}



//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions privées
//--------------------------------------------------------------------------------------------------

@end
