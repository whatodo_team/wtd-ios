//
//  CurrentContext.h
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

typedef void(^RequestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion)(BOOL userAuthorizeGeolocalisation, CLLocation *currentUserLocation);
typedef void(^OnInitialisationCompletion)(void);

@interface CurrentContext : NSObject
//Singleton vers une seule instance CurrentContext...
+ (CurrentContext *)sharedInstance;

//Initialisation du contexte...
- (void)initializeCurrentContextWithCompletion:(OnInitialisationCompletion)completion;

//Demande l'authorisation de géolocalisation...
- (void)requestGeolocalisationAuthorizationAndGetCurrentUserLocationWithCompletion:(RequestGeolocalisationAuthorizationAndGetCurrentUserLocationCompletion)completion;

@end
