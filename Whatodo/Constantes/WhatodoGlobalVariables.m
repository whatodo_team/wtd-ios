//
//  WhatodoGlobalVariables.m
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WhatodoGlobalVariables.h"
// Firebase
NSString *const kWTD_Firebase = @"https://whatodo.firebaseio.com/";
NSString *const kWTD_Firebase_Users = @"Users/";

// Storyboard
NSString *const kWTD_Storyboard_Login = @"Login";
NSString *const kWTD_Storyboard_Login_LoginVC = @"LoginViewController";
NSString *const kWTD_Storyboard_Login_SegueToHome = @"segueToHomeFromLogin";

NSString *const kWTD_Storyboard_Home = @"Home";
NSString *const kWTD_Storyboard_Home_HomeVC = @"HomeViewController";
NSString *const kWTD_Storyboard_Home_HomeTabBarVC = @"HomeTabBarViewController";
NSString *const kWTD_Storyboard_Home_ContainerVC = @"ContainerViewController";
NSString *const kWTD_Storyboard_Home_SideMenuVC = @"SideMenuViewController";
NSString *const kWTD_Storyboard_Home_SideMenuCellID = @"SideMenuCellID";

NSString *const kWTD_StoryboardActivity = @"Activity";
NSString *const kWTD_StoryboardActivity_ActivityDetail = @"ActivityDetailViewController";