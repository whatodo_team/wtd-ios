//
//  WhatodoGlobalVariables.h
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <Foundation/Foundation.h>
// Firebase
extern NSString *const kWTD_Firebase;
extern NSString *const kWTD_Firebase_Users;

// Storyboard
extern NSString *const kWTD_Storyboard_Login;
extern NSString *const kWTD_Storyboard_Login_LoginVC;
extern NSString *const kWTD_Storyboard_Login_SegueToHome;

extern NSString *const kWTD_Storyboard_Home;
extern NSString *const kWTD_Storyboard_Home_HomeVC;
extern NSString *const kWTD_Storyboard_Home_HomeTabBarVC;
extern NSString *const kWTD_Storyboard_Home_ContainerVC;
extern NSString *const kWTD_Storyboard_Home_SideMenuVC;
extern NSString *const kWTD_Storyboard_Home_SideMenuCellID;

extern NSString *const kWTD_StoryboardActivity;
extern NSString *const kWTD_StoryboardActivity_ActivityDetail;


