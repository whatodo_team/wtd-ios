//
//  WTDActivityMapAnnotation.m
//  Whatodo
//
//  Created by Valentin Denis on 17/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDActivityMapAnnotation.h"

@implementation WTDActivityMapAnnotation
//--------------------------------------------------------------------------------------------------
#pragma mark - Life cycle...
//--------------------------------------------------------------------------------------------------
//Initialisation...
- (instancetype)initWithActivity:(WTDActivity *)activity{
    self = [super init];
    if (self){
        _title = nil;
        _activity = activity;
        _coordinate = activity.location;
    }
    
    return self;
    
}
@end
