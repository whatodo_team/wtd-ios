//
//  WTDActivity.h
//  Whatodo
//
//  Created by Valentin Denis on 14/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface WTDActivity : NSObject
@property (nonatomic, assign) CLLocationCoordinate2D location;
@property (nonatomic, assign) NSUInteger activityID;
@property (nonatomic, strong) NSString *activitytitle;
@property (nonatomic, strong) NSString *activityDescription;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSArray *images;
@end
