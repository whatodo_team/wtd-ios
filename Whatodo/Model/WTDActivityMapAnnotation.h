//
//  WTDActivityMapAnnotation.h
//  Whatodo
//
//  Created by Valentin Denis on 17/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WTDActivityMapAnnotation : NSObject<MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) WTDActivity *activity;

- (instancetype)initWithActivity:(WTDActivity *)activity;

@end
