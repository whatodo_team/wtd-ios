//
//  WTDViewController.h
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface WTDViewController : UIViewController
- (void)customizeNavBar;

@end
