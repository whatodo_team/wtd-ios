//
//  WTDViewController.m
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDViewController.h"

@interface WTDViewController ()
@property (nonatomic, strong) UIImageView *logoImageView;

@end

@implementation WTDViewController

//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle
//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    //Remove de l'observer sur notifications center...
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Propriétés
//--------------------------------------------------------------------------------------------------
//Set title...
- (void)setTitle:(NSString *)title{
    [super setTitle:[title uppercaseString]];
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques...
//--------------------------------------------------------------------------------------------------
- (void)customizeNavBar{

    
}
@end
