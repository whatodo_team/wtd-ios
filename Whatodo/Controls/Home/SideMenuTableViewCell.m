//
//  SideMenuTableViewCell.m
//  Whatodo
//
//  Created by Valentin Denis on 26/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "SideMenuTableViewCell.h"

@implementation SideMenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
