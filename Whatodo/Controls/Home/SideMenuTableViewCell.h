//
//  SideMenuTableViewCell.h
//  Whatodo
//
//  Created by Valentin Denis on 26/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cellIcon;

@end
