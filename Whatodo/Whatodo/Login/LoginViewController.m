//
//  LoginViewController.m
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle
//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[CurrentContext sharedInstance] initializeCurrentContextWithCompletion:^{
        // Completion
        NSLog(@"Context initialisé");
    }];
    
    [[FacebookContext sharedInstance] initializeFacebookContextWithCompletion:^{
        // Completion
        if ([FacebookContext sharedInstance].isConnectedToFacebook) {
            [_avatarImage.layer setCornerRadius:_avatarImage.frame.size.width/2];
            [_avatarImage.layer setBorderWidth:1];
            [_avatarImage.layer setBorderColor:[UIColor blackColor].CGColor];
            [_avatarImage setClipsToBounds:YES];
            [_avatarImage sd_setImageWithURL:[NSURL URLWithString:[FacebookContext sharedInstance].userData.providerData[@"profileImageURL"]]];
            
            [self performSegueWithIdentifier:kWTD_Storyboard_Login_SegueToHome sender:nil];
        }else{
            NSLog(@"not connected to facebook");
        }
    }];
    //    // Test
    //    Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/test",kWTD_Firebase]];
    //    [[ref childByAutoId] setValue:@"test" withCompletionBlock:^(NSError *error, Firebase *ref) {
    //    }];
    //
    //    // Attach a block to read the data at our posts reference
    //    FirebaseHandle handle = [ref observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
    //        // Check si null
    //        if (snapshot.value == [NSNull null]) {
    //            // The value is null
    //        }else{
    //            NSLog(@"%@", snapshot.value);
    //        }
    //    } withCancelBlock:^(NSError *error) {
    //        NSLog(@"%@", error.description);
    //    }];
    //
    //    // Ne pas oublier de retirer l'observer, sinon ça continue à charger en mémoire même quand on quitte
    //    // le view controller, donc peut-être dans dealloc ? A ce moment là, ça dealloc aussi le ref ?
    //    [ref removeObserverWithHandle:handle];
    //
    //    // Ou alors on peut appeler un observer et le remove direct
    //    [ref observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
    //        // do some stuff once
    //    }];
    //
    //    Firebase *ref2 = [[Firebase alloc] initWithUrl:@"https://dinosaur-facts.firebaseio.com/dinosaurs"];
    //
    //    // Exemple query:
    //    [[ref2 queryOrderedByChild:@"dimensions/height"]
    //     observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
    //         NSLog(@"%@ was %@ meters tall", snapshot.key, snapshot.value[@"height"]);
    //     }];
    //
    //    // OU
    //
    //    [[ref2 queryOrderedByKey]
    //     observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
    //         NSLog(@"%@ was %@", snapshot.key, snapshot.value[@"height"]);
    //     }];
    //
    //    // Pour restreindre et faire une requête plus précise:
    //    [[[ref2 queryOrderedByChild:@"height"] queryLimitedToFirst:2]
    //     observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
    //         NSLog(@"%@", snapshot.key);
    //     }];
    //
    //    // Complex query
    //    [[[ref2 childByAppendingPath:@"stegosaurus"] childByAppendingPath:@"height"]
    //     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *stegosaurusHeightSnapshot) {
    //         NSNumber *favoriteDinoHeight = stegosaurusHeightSnapshot.value;
    //         FQuery *queryRef = [[[ref queryOrderedByChild:@"height"] queryEndingAtValue:favoriteDinoHeight] queryLimitedToLast:2];
    //         [queryRef observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *querySnapshot) {
    //             if (querySnapshot.childrenCount == 2) {
    //                 for (FDataSnapshot* child in querySnapshot.children) {
    //                     NSLog(@"The dinosaur just shorter than the stegasaurus is %@", child.key);
    //                     break;
    //                 }
    //             } else {
    //                 NSLog(@"The stegosaurus is the shortest dino");
    //             }
    //         }];
    //     }];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Navigation
//--------------------------------------------------------------------------------------------------

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


//--------------------------------------------------------------------------------------------------
#pragma mark - Actions
//--------------------------------------------------------------------------------------------------
- (IBAction)facebookLoginAction:(id)sender {
    // Si pas loggé
    [[FacebookContext sharedInstance] connectWithPermissions:@[@"email"] fromViewController:self withCompletion:^{
        // Go To Home
        [self performSegueWithIdentifier:kWTD_Storyboard_Login_SegueToHome sender:nil];
    }];
    
}
- (IBAction)noLoginAction:(id)sender {
    // Login as guest and go Home
    [[FirebaseContext sharedInstance].baseRef authAnonymouslyWithCompletionBlock:^(NSError *error, FAuthData *authData) {
        if (error) {
            // There was an error logging in anonymously
        } else {
            // We are now logged in
            [self performSegueWithIdentifier:kWTD_Storyboard_Login_SegueToHome sender:nil];

        }
    }];
}
@end
