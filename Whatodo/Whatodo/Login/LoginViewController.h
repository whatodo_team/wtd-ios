//
//  LoginViewController.h
//  Whatodo
//
//  Created by Valentin Denis on 24/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDViewController.h"

@interface LoginViewController : WTDViewController
@property (weak, nonatomic) IBOutlet UIButton *facebookLoginButton;
- (IBAction)facebookLoginAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *noLoginButton;
- (IBAction)noLoginAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;

@end
