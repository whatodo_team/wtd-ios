//
//  ActivityDetailViewController.h
//  Whatodo
//
//  Created by Valentin Denis on 22/04/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDViewController.h"

@interface ActivityDetailViewController : WTDViewController
@property (nonatomic, strong) WTDActivity *activity;
@end
