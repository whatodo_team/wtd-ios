//
//  ContainerViewController.h
//  Whatodo
//
//  Created by Valentin Denis on 26/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDViewController.h"

@interface ContainerViewController : WTDViewController
@property (nonatomic, strong) UITabBarController *mainTabBarController;
@property (weak, nonatomic) IBOutlet UIView *mainTabBarContainerView;

+ (instancetype)sharedInstance;
- (WTDViewController *)getMainViewController;

@end
