//
//  ContainerViewController.m
//  Whatodo
//
//  Created by Valentin Denis on 26/02/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "ContainerViewController.h"



@interface ContainerViewController (){
    CGFloat previousMenuDraggindOffset;
    BOOL isPushBackFlag;
    BOOL isFirstAppear;
}

- (void)initializeViewController;

@end

__strong static ContainerViewController *_sharedObject = nil;

@implementation ContainerViewController
//--------------------------------------------------------------------------------------------------
#pragma mark - Singleton...
//--------------------------------------------------------------------------------------------------
//Singleton vers une seule instance de Container...
+ (instancetype)sharedInstance{
    return _sharedObject;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    _sharedObject = self;
    isFirstAppear = YES;
    [self initializeViewController];
    
    isPushBackFlag = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques...
//--------------------------------------------------------------------------------------------------
//Récupère la view controller actuellement affichée par la tab bar...
- (WTDViewController *)getMainViewController{
    if (_mainTabBarController){
        UIViewController *vc = _mainTabBarController.selectedViewController;
        if ([vc isKindOfClass:[WTDViewController class]]){
            return (WTDViewController *)vc;
        }
    }
    return nil;
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions privées...
//--------------------------------------------------------------------------------------------------
- (void)initializeViewController{
    //Pour éviter que le back button item ait le titre de la vue de retour...
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:newBackButton];
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Navigation
//--------------------------------------------------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Inutile vu que le container est directement le VC de la Home
    if([segue.identifier isEqualToString:@"mainTabBar"]){
        self.mainTabBarController = segue.destinationViewController;

    }
    
}

@end
