//
//  MapViewController.m
//  Whatodo
//
//  Created by Valentin Denis on 15/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "MapViewController.h"
#import "WTDActivityMapAnnotation.h"

@interface MapViewController ()<MKMapViewDelegate>

- (void)initializeViewController;
@property (nonatomic, strong) NSMutableArray *activities;
@property (nonatomic, strong) NSMutableArray *annotations;

@end

@implementation MapViewController

//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle
//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeViewController];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions privées
//--------------------------------------------------------------------------------------------------
- (void)initializeViewController{
    //Map...
    _mapView.delegate = self;
    _activities = [[NSMutableArray alloc] init];
    _annotations = [[NSMutableArray alloc] init];
    
    __block MapViewController *weakSelf = self;
    [[CurrentContext sharedInstance] requestGeolocalisationAuthorizationAndGetCurrentUserLocationWithCompletion:^(BOOL userAuthorizeGeolocalisation, CLLocation *currentUserLocation) {
        if (userAuthorizeGeolocalisation) {
            [weakSelf getCloseActivitiesWithLocation:currentUserLocation];
        }
    }];
    
}

- (void)getCloseActivitiesWithLocation:(CLLocation *)currentUserLocation{
    for (int i = 0; i<=1; i++) {
        WTDActivity *activity = [WTDActivity new];
        activity.activitytitle = @"Test";
        activity.activityDescription = @"TestDescr";
        activity.activityID = i;
        activity.location = currentUserLocation.coordinate;
        WTDActivityMapAnnotation *annotation = [[WTDActivityMapAnnotation alloc] initWithActivity:activity];
        [_mapView addAnnotation:annotation];
    }
    
    [_mapView zoomToFitAnnotations];
}

//--------------------------------------------------------------------------------------------------
#pragma mark - MapViewDelegate...
//--------------------------------------------------------------------------------------------------
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]){
        
        return nil;
    }else if ([annotation isKindOfClass:[WTDActivityMapAnnotation class]])
    {
        WTDActivityMapAnnotation *wtdAnnotation = (WTDActivityMapAnnotation *)annotation;
        
        static NSString * const identifier = @"MyCustomAnnotation";
        MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView)
        {
            annotationView.annotation = annotation;
        }
        else
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:identifier];
        }
        annotationView.canShowCallout = NO;
        
        return annotationView;
    }
    return nil;

    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [mapView deselectAnnotation:view.annotation animated:YES];
    
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
