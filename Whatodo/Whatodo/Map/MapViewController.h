//
//  MapViewController.h
//  Whatodo
//
//  Created by Valentin Denis on 15/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDViewController.h"

@interface MapViewController : WTDViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
