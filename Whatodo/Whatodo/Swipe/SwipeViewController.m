//
//  SwipeViewController.m
//  Whatodo
//
//  Created by Valentin Denis on 14/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "SwipeViewController.h"
#import <ZLSwipeableView.h>
#import "CardContentView.h"

@interface SwipeViewController ()
@property (weak, nonatomic) IBOutlet ZLSwipeableView *swipeView;


@end

@implementation SwipeViewController

//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle...
//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    _swipeView.delegate = self;
    _swipeView.dataSource = self;

    self.view.backgroundColor = [UIColor lightGrayColor];
    
    //_swipeView.translatesAutoresizingMaskIntoConstraints = NO;

    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews {
    [_swipeView loadViewsIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions Privées
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
#pragma mark - ZLSwipeableView Delegates
//--------------------------------------------------------------------------------------------------
- (void)swipeableView:(ZLSwipeableView *)swipeableView
         didSwipeView:(UIView *)view
          inDirection:(ZLSwipeableViewDirection)direction {
    NSLog(@"did swipe in direction: %zd", direction);
    if (direction == ZLSwipeableViewDirectionLeft) {
        
    }else if(direction == ZLSwipeableViewDirectionRight){
        ActivityDetailViewController *activityDetailVC = [UIStoryboard getViewControllerWithIdentifier:kWTD_StoryboardActivity_ActivityDetail inStoryboardWithName:kWTD_StoryboardActivity];
        [self.navigationController pushViewController:activityDetailVC animated:YES];
    }
        
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView didCancelSwipe:(UIView *)view {
    NSLog(@"did cancel swipe");
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
  didStartSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    NSLog(@"did start swiping at location: x %f, y %f", location.x, location.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
          swipingView:(UIView *)view
           atLocation:(CGPoint)location
          translation:(CGPoint)translation {
    NSLog(@"swiping at location: x %f, y %f, translation: x %f, y %f", location.x, location.y,
          translation.x, translation.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
    didEndSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    NSLog(@"did end swiping at location: x %f, y %f", location.x, location.y);
}

//--------------------------------------------------------------------------------------------------
#pragma mark - ZLSwipeableView Datasource
//--------------------------------------------------------------------------------------------------
- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    CardView *view = [[CardView alloc] initWithFrame:swipeableView.bounds];
    view.backgroundColor = [UIColor redColor];
    
    CardContentView *contentView =
    [[NSBundle mainBundle] loadNibNamed:@"CardContentView" owner:self options:nil][0];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:contentView];

    NSDictionary *metrics =
    @{ @"height" : @(view.bounds.size.height),
       @"width" : @(view.bounds.size.width) };
    NSDictionary *views = NSDictionaryOfVariableBindings(contentView);
    [view addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"H:|[contentView(width)]"
                          options:0
                          metrics:metrics
                          views:views]];
    [view addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:|[contentView(height)]"
                          options:0
                          metrics:metrics
                          views:views]];
    
    
    

    return view;

}

@end
