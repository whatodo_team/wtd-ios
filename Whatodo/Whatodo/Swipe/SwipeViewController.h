//
//  SwipeViewController.h
//  Whatodo
//
//  Created by Valentin Denis on 14/03/2016.
//  Copyright © 2016 Whatodo Team. All rights reserved.
//

#import "WTDViewController.h"
#import <ZLSwipeableView.h>
#import "CardView.h"

@interface SwipeViewController : WTDViewController<ZLSwipeableViewDataSource, ZLSwipeableViewDelegate>
@end
